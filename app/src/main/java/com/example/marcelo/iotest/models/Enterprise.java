package com.example.marcelo.iotest.models;

import com.google.gson.annotations.SerializedName;

public class Enterprise {

    @SerializedName("photo")
    private String photo;
    @SerializedName("enterprise_name")
    private String name;
    @SerializedName("enterprise_type_name")
    private String type;
    @SerializedName("country")
    private String country;
    @SerializedName("description")
    private String description;
    @SerializedName("id")
    private int id;


    public Enterprise() { }


    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
