package com.example.marcelo.iotest.ui;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.marcelo.iotest.R;

public class DetailingActivity extends AppCompatActivity {

    private String enterpriseName;
    private String enterprisePhoto;
    private String enterpriseDescription;
    private ImageView backArrow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detailing);
        getIncomingIntent();
        initWidgets();
        backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();
            }
        });

    }

    private void getIncomingIntent() {

        enterpriseName = getIntent().getStringExtra("ENTERPRISE_NAME");
        enterprisePhoto = getIntent().getStringExtra("ENTERPRISE_PHOTO");
        enterpriseDescription = getIntent().getStringExtra("ENTERPRISE_DESCRIPTION");

    }

    private void initWidgets() {

        backArrow = findViewById(R.id.ic_arrow_back);
        TextView mEnterpriseName = findViewById(R.id.enterprise_name);
        ImageView mEnterprisePhoto = findViewById(R.id.enterprise_photo);
        TextView mEnterpriseDescription = findViewById(R.id.enterprise_description);
        mEnterpriseDescription.setMovementMethod(new ScrollingMovementMethod());
        mEnterpriseName.setText(enterpriseName);

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.error(R.drawable.img_not_found);
        Glide.with(this)
                .setDefaultRequestOptions(requestOptions)
                .asBitmap()
                .load(enterprisePhoto)
                .apply(requestOptions)
                .into(mEnterprisePhoto);
        mEnterpriseDescription.setText(enterpriseDescription);

    }
}
