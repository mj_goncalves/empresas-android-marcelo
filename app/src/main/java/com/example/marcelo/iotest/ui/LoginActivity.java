package com.example.marcelo.iotest.ui;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.marcelo.iotest.R;
import com.example.marcelo.iotest.helpers.ConnectivityHelper;
import com.example.marcelo.iotest.helpers.SharedPreferencesHelper;
import com.example.marcelo.iotest.models.User;
import com.example.marcelo.iotest.rest.APIClient;
import com.example.marcelo.iotest.rest.AccessEndPoints;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {
    private Button btnLogin;
    private EditText mEmail, mPassword;
    private ProgressBar progressBar;
    private String token, client, uid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initWidgets();
        setupLogin();
    }

    private void setupLogin() {

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String email = mEmail.getText().toString();
                String password = mPassword.getText().toString();

                if (email.isEmpty() | password.isEmpty()) {

                    Toast.makeText(LoginActivity.this, getString(R.string.fill_fields), Toast.LENGTH_SHORT)
                            .show();

                } else if (ConnectivityHelper.checkIfConnectionIsOffline(LoginActivity.this)) {

                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(LoginActivity.this, getString(R.string.check_internet_connection), Toast.LENGTH_SHORT)
                            .show();

                } else {

                    User user = new User(email, password);
                    sendHeadersRequest(user);

                }
            }
        });
    }

    private void sendHeadersRequest(User user) {


        progressBar.setVisibility(View.VISIBLE);

        AccessEndPoints apiService = APIClient.getClient().create(AccessEndPoints.class);
        Call<User> call = apiService.sendUserData(user);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(@NonNull Call<User> call, @NonNull Response<User> response) {
                if (response.isSuccessful()) {

                    token = response.headers().get("access-token");
                    client = response.headers().get("client");
                    uid = response.headers().get("uid");
                    SharedPreferencesHelper sharedPreferencesHelper = new SharedPreferencesHelper(LoginActivity.this);
                    sharedPreferencesHelper.saveToSharedPreferences(token, client, uid);
                    openHomeActivity();

                } else {

                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(LoginActivity.this, getString(R.string.check_login_info), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<User> call, @NonNull Throwable t) {

                progressBar.setVisibility(View.GONE);
                Toast.makeText(LoginActivity.this, getString(R.string.fatal_error), Toast.LENGTH_SHORT)
                        .show();
            }
        });
    }

    private void openHomeActivity() {

        progressBar.setVisibility(View.GONE);
        mEmail.setText("");
        mPassword.setText("");
        Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
        startActivity(intent);
    }

    private void initWidgets() {

        mEmail = findViewById(R.id.email);
        mPassword = findViewById(R.id.password);
        btnLogin = findViewById(R.id.login);
        progressBar = findViewById(R.id.progress_bar);
        progressBar.setVisibility(View.GONE);

    }
}
