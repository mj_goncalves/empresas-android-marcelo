package com.example.marcelo.iotest.rest;

import com.example.marcelo.iotest.models.User;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface AccessEndPoints {

    @POST("users/auth/sign_in")
    Call<User> sendUserData(@Body User user);

    @GET("enterprises")
    Call<Object> getAccessData(@Header("access-token") String accessToken,
                               @Header("client") String client,
                               @Header("uid") String uid);
}
