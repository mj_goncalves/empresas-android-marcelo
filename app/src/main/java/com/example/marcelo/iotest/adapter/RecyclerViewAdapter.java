package com.example.marcelo.iotest.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.marcelo.iotest.R;
import com.example.marcelo.iotest.models.Enterprise;
import com.example.marcelo.iotest.ui.DetailingActivity;

import java.util.ArrayList;
import java.util.List;


    public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.EnterprisesViewHolder> {
        private List<Enterprise> enterprisesList;
        private List<Enterprise> enterprisesHelperList = new ArrayList<>();
        private AdapterCallback mAdapterCallback;
        private Context context;
        private int rowLayout;

        public RecyclerViewAdapter(List<Enterprise> enterpriseList, Context context, int rowLayout) {
            this.enterprisesList = enterpriseList;
            this.context = context;
            this.rowLayout = rowLayout;
            this.enterprisesHelperList.addAll(enterpriseList);
            this.enterprisesList.clear();
            this.mAdapterCallback = ((AdapterCallback) context);
        }

        @NonNull
        @Override
        public EnterprisesViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

            View view = LayoutInflater.from(context).inflate(rowLayout, viewGroup, false);
            return new EnterprisesViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull final EnterprisesViewHolder enterprisesViewHolder, int i) {

            String PHOTO_BASE_URL = "http://empresas.ioasys.com.br";
            final String photoURL = PHOTO_BASE_URL + enterprisesList.get(i).getPhoto();

            RequestOptions requestOptions = new RequestOptions();
            requestOptions.error(R.drawable.img_not_found);

            Glide.with(context)
                    .setDefaultRequestOptions(requestOptions)
                    .asBitmap()
                    .load(photoURL)
                    .apply(requestOptions)
                    .into(enterprisesViewHolder.holderEnterprisePhoto);
            enterprisesViewHolder.holderEnterpriseName.setText(enterprisesList.get(i).getName());
            enterprisesViewHolder.holderEnterpriseType.setText(enterprisesList.get(i).getType());
            enterprisesViewHolder.holderEnterpriseCountry.setText(enterprisesList.get(i).getCountry());

            enterprisesViewHolder.holderLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent = new Intent(context, DetailingActivity.class);
                    intent.putExtra("ENTERPRISE_NAME", enterprisesList.get(enterprisesViewHolder.getAdapterPosition())
                            .getName());
                    intent.putExtra("ENTERPRISE_PHOTO", photoURL);
                    intent.putExtra("ENTERPRISE_DESCRIPTION", enterprisesList.get(enterprisesViewHolder.getAdapterPosition())
                            .getDescription());
                    context.startActivity(intent);
                }
            });
        }

        @Override
        public int getItemCount() {
            return enterprisesList.size();
        }


        static class EnterprisesViewHolder extends RecyclerView.ViewHolder{


            LinearLayout holderLayout;
            ImageView holderEnterprisePhoto;
            TextView holderEnterpriseName;
            TextView holderEnterpriseType;
            TextView holderEnterpriseCountry;


            EnterprisesViewHolder(@NonNull View itemView) {
                super(itemView);

                holderLayout = itemView.findViewById(R.id.row_layout);
                holderEnterprisePhoto = itemView.findViewById(R.id.enterprise_photo);
                holderEnterpriseName = itemView.findViewById(R.id.enterprise_name);
                holderEnterpriseType = itemView.findViewById(R.id.enterprise_type);
                holderEnterpriseCountry = itemView.findViewById(R.id.enterprise_country);
            }
        }

        private void clearList(){

            enterprisesList.clear();

        }
        public void loadEnterprisesList(String s){

            clearList();
            String stringToBeSearched = s.toLowerCase();

            if (stringToBeSearched.length() != 0){

                for (Enterprise enterprise: enterprisesHelperList){

                    String enterpriseName = enterprise.getName().toLowerCase();

                    if (enterpriseName.contains(stringToBeSearched)){

                        enterprisesList.add(enterprise);
                    }
                }
            }

            notifyDataSetChanged();
            mAdapterCallback.onLoadEnterprises();
        }

        public static interface AdapterCallback {
            void onLoadEnterprises();
        }
}
