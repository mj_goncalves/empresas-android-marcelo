package com.example.marcelo.iotest.ui;

import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.marcelo.iotest.R;
import com.example.marcelo.iotest.adapter.RecyclerViewAdapter;
import com.example.marcelo.iotest.helpers.ConnectivityHelper;
import com.example.marcelo.iotest.models.Enterprise;
import com.example.marcelo.iotest.rest.APIClient;
import com.example.marcelo.iotest.rest.AccessEndPoints;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends AppCompatActivity implements RecyclerViewAdapter.AdapterCallback{

    private TextView startSearch;
    private String token, client, uID;
    private List<Enterprise> mEnterprisesList = new ArrayList<>();
    private RecyclerViewAdapter recyclerViewAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        startSearch = findViewById(R.id.start_search);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(false);
        getHeaderData();
        loadEnterprises();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.home_menu, menu);
        SearchView searchView = (SearchView) menu.findItem(R.id.ic_action_search).getActionView();
        searchView.setQueryHint(getString(R.string.search));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;

            }

            @Override
            public boolean onQueryTextChange(String s) {

                if (s.isEmpty()) {

                    recyclerViewAdapter.loadEnterprisesList("");


                } else if (s.length() >= 3) {

                    recyclerViewAdapter.loadEnterprisesList(s);

                }

                return false;
            }
        });
        return true;
    }

    private void getHeaderData() {

        SharedPreferences sharedPreferences = getSharedPreferences("accessData", MODE_PRIVATE);
        String DEFAULT = "N/A";
        token = sharedPreferences.getString("TOKEN", DEFAULT);
        client = sharedPreferences.getString("CLIENT", DEFAULT);
        uID = sharedPreferences.getString("UID", DEFAULT);

    }

    private void loadEnterprises() {

        if (ConnectivityHelper.checkIfConnectionIsOffline(this)) {

            Toast.makeText(this, getString(R.string.check_login_info), Toast.LENGTH_SHORT).show();

        } else {

            AccessEndPoints apiService = APIClient.getClient().create(AccessEndPoints.class);
            Call<Object> call = apiService.getAccessData(token, client, uID);
            call.enqueue(new Callback<Object>() {
                @Override
                public void onResponse(@NonNull Call call, @NonNull Response response) {

                    Gson gson = new Gson();
                    String json = gson.toJson(response.body());
                    mEnterprisesList = getEnterprisesList(json);
                    setupRecyclerView();
                }

                @Override
                public void onFailure(@NonNull Call call, @NonNull Throwable t) {

                    Toast.makeText(HomeActivity.this, R.string.fatal_error, Toast.LENGTH_SHORT)
                            .show();
                }
            });
        }
    }

    private void setupRecyclerView() {

        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewAdapter = new RecyclerViewAdapter(mEnterprisesList, this, R.layout.layout_recycler_view_row);
        recyclerView.setAdapter(recyclerViewAdapter);
    }

    public List<Enterprise> getEnterprisesList(String json) {

        List<Enterprise> enterprises = new ArrayList<>();
        try {

            JSONObject jsonObject = new JSONObject(json);
            JSONArray jsonArray = jsonObject.getJSONArray("enterprises");
            JSONObject JOEntreprise;

            for (int i = 0; i < jsonArray.length(); i++) {

                JOEntreprise = new JSONObject(jsonArray.getString(i));

                Enterprise enterprise = new Enterprise();

                if (JOEntreprise.has("photo")) {

                    enterprise.setPhoto(JOEntreprise.getString("photo"));
                }
                enterprise.setName(JOEntreprise.getString("enterprise_name"));
                enterprise.setType(JOEntreprise.getJSONObject("enterprise_type")
                        .getString("enterprise_type_name"));
                enterprise.setCountry(JOEntreprise.getString("country"));
                enterprise.setDescription(JOEntreprise.getString("description"));
                enterprise.setId(Integer.parseInt(JOEntreprise.getString("id")));
                enterprises.add(enterprise);

            }

        } catch (JSONException e) {

            e.printStackTrace();
        }

        return enterprises;

    }

    @Override
    public void onLoadEnterprises() {

        if (recyclerViewAdapter.getItemCount() == 0){

            startSearch.setVisibility(View.VISIBLE);

        }else {

            startSearch.setVisibility(View.GONE);

        }
    }
}
